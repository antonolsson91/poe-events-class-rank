// ==UserScript==
// @name         PoE Events: Class Rank
// @namespace    https://github.com/eagl3s1ght
// @version      1.1.0
// @description  Displays the class-specific ranks on Ladder pages.
// @require      http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js
// @include      http://www.pathofexile.com/forum/view-thread/*
// @include      http://www.pathofexile.com/seasons

// @copyright    2013, Eagl3s1ght
// ==/UserScript==

/*! Author: Robert Hashemian http://www.hashemian.com/ You can use this code in any manner so long as the author's name, Web address and this disclaimer is kept intact. */
function FormatNumberBy3(num,decpoint,sep){if(arguments.length==2)sep=",";if(arguments.length==1){sep=",";decpoint="."}num=num.toString();a=num.split(decpoint);x=a[0];y=a[1];z="";if(typeof x!="undefined"){for(i=x.length-1;i>=0;i--)z+=x.charAt(i);z=z.replace(/(\d{3})/g,"$1"+sep);if(z.slice(-sep.length)==sep)z=z.slice(0,-sep.length);x="";for(i=z.length-1;i>=0;i--)x+=z.charAt(i);if(typeof y!="undefined"&&y.length>0)x+=decpoint+y}return x};

scriptName = '<small>PoE Events: Class Rank</small>';
scriptRepeat = 2500; //ms

$( document ).ready(function() { // Handler for .ready() called.
    /*! Desktop Notifications for the Event Schedule (pathofexile.com/seasons) */
    if( $(location).attr('pathname').indexOf('seasons') > -1 ){

        currentDate = new Date();
        currentDate.unix = currentDate.getTime();
        currentDate.ymd = currentDate.getFullYear() + '-' + (currentDate.getMonth() + 1) + '-' + currentDate.getDate() + ' ';
        
        /*! WebKit Notification Permissions */
        $('#eventCalendar').css('position', 'relative');
        $('<button id="enable_notifications"></button><style>#enable_notifications{ opacity: 0.9; margin: 4px; z-index: 10000; position: absolute; top: 0px; right: 0px; color: #000 !important; display: inline-block;background: linear-gradient(rgb(249, 249, 249) 40%, rgb(227, 227, 227) 70%);border: 1px solid rgb(153, 153, 153);border-image-source: initial;border-image-slice: initial;border-image-width: initial;border-image-outset: initial;border-image-repeat: initial;border-radius: 3px;padding: 5px 8px;outline: 0px;white-space: nowrap;-webkit-user-select: none;cursor: pointer;text-shadow:  rgb(255, 255, 255) 1px 1px;font-weight: 700;font-size: 10pt; } #enable_notifications:hover{ opacity: 1; } #enable_notifications[disabled] {opacity: 0.2 !important;}</style>').appendTo('#eventCalendar'); 
        $('#enable_notifications').toggle();
        
        if (window.webkitNotifications.checkPermission() !== 0) {
            $('#enable_notifications').prop("disabled", "false");
            $('#enable_notifications').html('Desktop Notification Permissions are required.'); 
        } else {
            $('#enable_notifications').html('Desktop Notification Permissions have been set.');
            $('#enable_notifications').prop("disabled", "true");
        } 
        $('#enable_notifications').toggle();

        $('#enable_notifications').on('click', function () {
            $(this).fadeOut("slow", function () {
                $(this).hide();
            });
            window.webkitNotifications.requestPermission();
        });
        
        /*! WebKit Notification Creation */
        function notify(title, desc, link) {
            var havePermission = window.webkitNotifications.checkPermission();

            var notification = window.webkitNotifications.createNotification(
                'http://puu.sh/4KFal/12c03a2668.png',
            title,
            desc);

            notification.onclick = function () {
                window.open(link);
                notification.close();
                
                $('#enable_notifications').html('Desktop Notification Permissions have been set.');
                $('#enable_notifications').prop("disabled", "true");
            };
            notification.show();
        }
        
        /*! 
            Start One-second intervals for each event which checks if the event is within X minutes of currentTime.
            When that occurs, call notify() and remove the fired interval.
        */
        var intervals = [];
        $('.day').each(function (dayIndex) {
            var eventDay = $(this).children('.label').html();

            $(this).find('.periods .period .event').each(function (index) {
                currentDate.ymd = currentDate.getFullYear() + '-' + (currentDate.getMonth() + 1) + '-' + eventDay + ' ';
                var eventName = $(this).children('a').html();
                var eventLink = $(this).children('a').attr('href');

                switch ($(this).parent().attr('class').split(' ')[1]) {
                    case 'period0':
                        eventDate = new Date(currentDate.ymd + '01:00');
                        break;
                    case 'period1':
                        eventDate = new Date(currentDate.ymd + '04:00');
                        break;
                    case 'period2':
                        eventDate = new Date(currentDate.ymd + '08:00');
                        break;
                    case 'period3':
                        eventDate = new Date(currentDate.ymd + '11:00');
                        break;
                    case 'period4':
                        eventDate = new Date(currentDate.ymd + '13:00');
                        break;
                    case 'period5':
                        eventDate = new Date(currentDate.ymd + '15:00');
                        break;
                    case 'period6':
                        eventDate = new Date(currentDate.ymd + '18:00');
                        break;
                    case 'period7':
                        eventDate = new Date(currentDate.ymd + '22:00');
                        break;
                }

                var minutes_before_event_to_notify = 15;
                eventDate.setTime(eventDate.getTime() - ((minutes_before_event_to_notify * 60) * 1000));
                eventDate.unix = eventDate.getTime();

                intervals[index] = setInterval(function () {
                    if (currentDate.unix >= eventDate.unix) {
                        console.log(currentDate.unix + '<=' + eventDate.unix);
                        notify(eventName, "Event starts in " + minutes_before_event_to_notify + " minutes. Click here to go to event thread.", eventLink);
                        clearInterval(intervals[index]);
                    }
                }, 1 * 1000);
            });
        });
    }
    
        
    if ( !$('.countdownContainer').html() && !$('.finishedMessage').html() ){
        console.log(scriptName + " This forum thread is not an event page.");
                
        (function main() {

            scriptSinceStart = (typeof scriptSinceStart === 'undefined') ? 0 : scriptSinceStart+scriptRepeat;

            // Remove old script-injections.
            $('.classRank_th, .classRank, #classDistribution, audio').each(function () {
                $(this).remove();
            });

            //If user is logged in, check for character name and highlight on ladder.
            var accountName = ($('#statusBar div span span.profile-link a').html()) ? $('#statusBar div span span.profile-link a').html() : null;
            var classNum;
            var ranks = [0, 0, 0, 0, 0, 0];
            var eventParticipants = 0;
            var classDistribution = [0, 0, 0, 0, 0, 0];
            var classNames = ["Ranger", "Shadow", "Duelist", "Witch", "Templar", "Marauder"];
            var classPics = ["http://puu.sh/4zOmR/9196bba266.jpg", "http://puu.sh/4zOn3/c66407a371.jpg", "http://puu.sh/4zOmo/784d5b25dd.jpg", "http://puu.sh/4zOoZ/5054011ca5.jpg", "http://puu.sh/4zOmE/9193d5fd71.jpg", "http://puu.sh/4zOjb/1521bb4314.jpg"];
            var classRank;
            var accountRank, accountClassRank, accountClass;

            // Some features won't be accurate if Player Rank 1 is not on the page.
            if ( scriptSinceStart > 5000 && $('tbody.entries>.entry:nth-child(1)>.rank').html() != 1 && !$('.countdownContainer').html() ){
               $('body').append('<div id="poe_cr_warning" style="width: 600px; text-align: center; border: 1px solid red; border-radius: 4px; background: black; padding: 8px; z-index: 5000; font-size: 16px; position: fixed; top: 10%; left: 50%; margin-left: -300px;"><strong>'+scriptName+'</strong> will not work if you are not seeing Event Rank #1 on your page.</div>'); 
               $('#poe_cr_warning').fadeOut( scriptRepeat-200 , function(){ $(this).remove(); } );
               return false;
            }

            $('tr.entry').each(function () {
                var charName = $(this).children('td.character').html();
                var exp = $(this).children('td.experience').html();
                var level = $(this).children('td.level').html();
                var rank = $(this).children('td.rank').html();
                var char_class = $(this).children('td.class').html();
                var account = $(this).children('td.account').children('span').children('a').not('img').html();        
                
                // Thousand-separator for Experience Values (suggested by sunday_evening)
                if( exp.indexOf(',') == -1 )
                    $(this).children('td.experience').html( FormatNumberBy3(exp, ".", ",") );
                
                switch (char_class) {
                    case "Ranger":
                        classNum = 0;
                        break;
                    case "Shadow":
                        classNum = 1;
                        break;
                    case "Duelist":
                        classNum = 2;
                        break;
                    case "Witch":
                        classNum = 3;
                        break;
                    case "Templar":
                        classNum = 4;
                        break;
                    case "Marauder":
                        classNum = 5;
                        break;
                }

                ranks[classNum]++;
                classDistribution[classNum]++;
                classRank = ranks[classNum];
                eventParticipants++;

                // Highlight own account entry + save rank information.
                if (accountName == account) {
                    $(this).addClass('ownAccount'); $(".ownAccount").css({
                        'border': '1px solid gray',
                        'background': 'rgba(0,0,0,0.7)'
                    });

                    accountRank = rank;
                    accountClassRank = classRank;
                    accountClass = char_class;
                }
                
                $(this).append('<td class="classRank" style="text-align: right;">#' + classRank + ' <img src="' + classPics[classNum] + '" /></td>');
            }); $('table thead tr').append('<th class="classRank_th" style="text-align: right;">Class Rank</th>');

            // Class Distribution
            $('body').append('<div id="classDistribution" style="position: fixed; bottom: 0px; right: 0px; z-index: 5000; padding: 2px; margin: 8px; border: 1px solid #888; "><div style="text-decoration: underline;text-align: center;">'+scriptName+' Data Stack: '+eventParticipants+'</div><ul></ul></div>');
            for (var i = 0; i < classNames.length; i++) {
                $('<li>' + classDistribution[i] + ' ' + classNames[i] + ((classNames[i] != "Witch") ? "s" : "es") + ' (' + ((eventParticipants/100)*classDistribution[i]).toFixed(2) + '%)</li>').appendTo('#classDistribution ul');
            }

            // Play sounds for Event / Starting | Ending | Ladder Rank | Class Rank | Current Points
            //$('<audio id="audioPlayer" controls style="position: fixed; top: 0px; right: 0px; z-index: 5000; padding: 2px; margin: 8px; border: 1px solid #888; "><source src="http://www.ppcec.org/vox/Kenya/I\'m%20gonna%20walk.mp3"></audio>').appendTo('body'); $.get();
            

            // Race has ended? Show points gained && Final rank
            if ( $('body').find(".finishedMessage") ){
                if( $('body').find("#poee_finishedMessage") ){ $('#poee_finishedMessage').remove(); }

                if( accountClass && accountClassRank && accountRank )
                    $('.finishedMessage').html( $('.finishedMessage').html() + '<div id="poee_finishedMessage" style="margin: 8px 5px 0px 5px; padding-top: 8px; border-top: 1px solid rgba(255,255,255,0.1);"><strong>'+scriptName+':</strong> You finished on Rank '+accountRank+' as '+accountClass+' #'+accountClassRank+'. Congratulations!</div>');
                else
                    $('.finishedMessage').html( $('.finishedMessage').html() + '<div id="poee_finishedMessage" style="margin: 8px 5px 0px 5px; padding-top: 8px; border-top: 1px solid rgba(255,255,255,0.1);"><strong>'+scriptName+':</strong> Can\'t find you on this page, sorry. Try showing more entries per page.</div>');
            }
            
            setTimeout(main, scriptRepeat);
        })();
    }

});

