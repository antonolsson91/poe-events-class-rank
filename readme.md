## PoE-Events: Class Rank

Displays the class-specific ranks on Ladder pages.

## Install

Available as a [Userscript](https://github.com/eagl3s1ght/PoE-Events-Class-Rank/raw/master/.extensions/userscript/main.user.js) for browsers with support.
Only tested in Google Chrome with [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo).

### Overview:

![screenshot](http://puu.sh/4AdIa/e29925657d.png)

![screenshot](http://puu.sh/4AdJB/c3813b5b59.png)

## Features
 
- **Adds a column on the ladder with every characters Class Rank.** 
- Highlights your character on the Ladder. 
- Summarizes the event when it has ended with your event rank & class rank. (Requires you to be logged in)
- Event Class distribution (only first page) (*Experimental!*)
- (*Not yet implemented!*) Audio announcements when event starts/ends/your current ladder rank/class rank/current points gained.

Powered by jQuery, Adobe Fireworks ([R.I.P](http://blogs.adobe.com/fireworks/2013/05/the-future-of-adobe-fireworks.html)) and [Path of Exile, an online Action RPG set in a dark fantasy world](http://www.pathofexile.com/).

## Feedback, Contributions & Questions

Feel free to send me feedback, you can contact me on <eaglesight@comhem.se> or simply start forking if you wish to contribute!
To test the extension locally, simply open main.user.js with your browser and whatever userscript manager you have installed should ask you if you want to install.

Check out my other projects @ [Github - Repositories](https://github.com/eagl3s1ght?tab=repositories)!